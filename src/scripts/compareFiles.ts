import { PersonDTO } from "../models";
import { parsePersonTXT, parsePersonXML } from "../parsers";
import { DATA_1_PATH, DATA_2_PATH } from "../constants";

function comparePersonArrays(persons_a: PersonDTO[], persons_b: PersonDTO[]) {
  let index_a = 0;
  let index_b = 0;

  const diff_report: {
    only_in_a: number[];
    only_in_b: number[];
    object_diffs: FieldDiffs[];
  } = {
    only_in_a: [],
    only_in_b: [],
    object_diffs: [],
  };

  const a_sorted = [...persons_a].sort(sortPersonById);
  const b_sorted = [...persons_b].sort(sortPersonById);

  while (index_a < a_sorted.length && index_b < b_sorted.length) {
    const p_a = a_sorted[index_a];
    const p_b = b_sorted[index_b];

    const id_a = p_a.PersonId;
    const id_b = p_b.PersonId;

    if (id_a < id_b) {
      diff_report.only_in_a.push(id_a);
      index_a++;
    } else if (id_a > id_b) {
      diff_report.only_in_b.push(id_b);
      index_b++;
    } else {
      const object_diff = getObjectDiff(p_a, p_b);
      if (object_diff) {
        diff_report.object_diffs.push(object_diff);
        index_a++;
        index_b++;
      }
    }
  }

  if (index_a < a_sorted.length) {
    a_sorted
      .slice(index_a)
      .map(({ PersonId }) => PersonId)
      .forEach((id) => diff_report.only_in_a.push(id));
  }

  if (index_b < b_sorted.length) {
    b_sorted
      .slice(index_b)
      .map(({ PersonId }) => PersonId)
      .forEach((id) => diff_report.only_in_b.push(id));
  }
  return diff_report;
}

interface FieldDiffs {
  PersonId: number;
  a: Partial<PersonDTO>;
  b: Partial<PersonDTO>;
}

function getObjectDiff(a: PersonDTO, b: PersonDTO): FieldDiffs | false {
  const a_diff: Partial<PersonDTO> = {};
  const b_diff: Partial<PersonDTO> = {};

  let has_diff = false;

  Object.keys(a).forEach((attribute) => {
    const a_value = a[attribute];
    const b_value = b[attribute];

    if (a[attribute] !== b[attribute]) {
      has_diff = true;
      a_diff[attribute] = a_value;
      b_diff[attribute] = b_value;
    }
  });

  if (has_diff) {
    return {
      PersonId: a.PersonId,
      a: a_diff,
      b: b_diff,
    };
  } else {
    return false;
  }
}

const sortPersonById = (person_a: PersonDTO, person_b: PersonDTO) =>
  person_a.PersonId - person_b.PersonId;

async function compare() {
  const persons_a = parsePersonTXT(DATA_1_PATH);
  const persons_b = parsePersonXML(DATA_2_PATH);

  const res = comparePersonArrays(persons_a, persons_b);
  console.log(JSON.stringify(res, null, 2));
}

compare();
