import { parseCommentsTXT, parsePersonXML } from "../parsers";
import { Person, CommentDTO } from "../models";
import { DATA_3_PATH, DATA_2_PATH } from "../constants";

function getComments(): CommentDTO[] {
  return parseCommentsTXT(DATA_3_PATH);
}

function getPersons(): Person[] {
  return parsePersonXML(DATA_2_PATH).map((dto) => new Person(dto));
}

function addCommentsToPersons({
  comments,
  persons,
}: {
  comments: CommentDTO[];
  persons: Person[];
}) {
  comments.forEach((c) => {
    const person = persons.find(
      (person) => person.getPersonId() === c.PersonId
    );
    person && person.addComment(c);
  });
}

function sortPersonsByCommentsCount(persons: Person[]) {
  persons.sort((a, b) => b.getNumberOfComments() - a.getNumberOfComments());
}

function printNameAndCommentsCount(p: Person) {
  console.log(`${p.getFullName()}, no of comments: ${p.getNumberOfComments()}`);
}

function PrintPersonsByComments() {
  const comments = getComments();
  const persons = getPersons();

  addCommentsToPersons({ comments, persons });

  sortPersonsByCommentsCount(persons);

  persons.forEach(printNameAndCommentsCount);
}

PrintPersonsByComments();
