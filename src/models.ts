export class Person {
  private Employments: Employment[] = [];
  private CommentDTOs: CommentDTO[] = [];
  private PersonDTO: PersonDTO;
  constructor(PersonDTO: PersonDTO) {
    this.PersonDTO = { ...PersonDTO };
  }

  getPersonId(): number {
    return this.PersonDTO.PersonId;
  }

  addEmployment(e: Employment) {
    this.Employments.push(e);
  }

  getEmployments(): Employment[] {
    return [...this.Employments];
  }

  getFirstName(): string {
    return this.PersonDTO.FirstName;
  }
  getLastName(): string {
    return this.PersonDTO.LastName;
  }

  getFullName(): string {
    return `${this.getFirstName()} ${this.getLastName()}`;
  }

  addComment(comment: CommentDTO) {
    this.CommentDTOs.push(comment);
  }

  getCommentDTOs(): CommentDTO[] {
    return [...this.CommentDTOs];
  }

  getNumberOfComments(): number {
    return this.CommentDTOs.length;
  }
  getPersonDTO(): PersonDTO {
    return { ...this.PersonDTO };
  }
}

export class Employment {
  private EmploymentDTO: EmploymentDTO;
  private CompanyDTO: CompanyDTO;

  constructor(employment: EmploymentDTO, company: CompanyDTO) {
    this.EmploymentDTO = employment;
    this.CompanyDTO = company;
  }

  getCompanyName() {
    return this.CompanyDTO.CompanyName;
  }
  getStartDate() {
    return this.EmploymentDTO.StartDate;
  }
  getEndDate() {
    return this.EmploymentDTO.EndDate;
  }
  getEmploymentDTO() {
    return { ...this.EmploymentDTO };
  }
  getCompanyDTO() {
    return { ...this.CompanyDTO };
  }
}

export interface CompanyDTO {
  CompanyID: number;
  CompanyName: string;
}

export interface CommentDTO {
  CommentId?: number;
  PersonId: number;
  Comment: string;
}

export interface PersonDTO {
  PersonId: number;
  FirstName: string;
  LastName: string;
  Age: number;
  Color: string;
}

export interface EmploymentDTO {
  EmploymentId?: number;
  PersonId: number;
  CompanyId: number;
  StartDate: Date;
  EndDate?: Date;
}
