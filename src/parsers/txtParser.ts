import { readFileSync } from "fs";

export type txtParserParams = {
  path: string;
  columns: string[];
  expected_header: string;
  field_separator: string;
  field_parsers: { [key: string]: (value: string) => any };
};

export default function txtParser(params: txtParserParams): any[] {
  const { path, field_separator, expected_header } = params;
  const file = readFileSync(path);
  const lines = file.toString().trim().split(/\r?\n/);

  const [header, ...rows] = lines;

  const is_expected_header = header === expected_header;

  if (!is_expected_header) {
    const err_msg = `Unexpected header. Received "${header}". Expected "${expected_header}"`;
    throw err_msg;
  }

  const parsed_lines: any[] = [];

  rows.forEach((row, index) => {
    const parsed_fields = parseFields(row.split(field_separator), params);
    parsed_lines.push(parsed_fields);
  });

  return parsed_lines;
}

function parseFields(fields: string[], opts: txtParserParams): any {
  const row_res = {};
  const { field_parsers, columns } = opts;
  fields.forEach((value, index) => {
    const field_name = columns[index];
    const parser = field_parsers[field_name];

    row_res[field_name] = parser(value);
  });
  return row_res;
}
