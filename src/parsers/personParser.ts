import txtParser, { txtParserParams } from "./txtParser";

import { PersonDTO } from "../models";

import { readFileSync } from "fs";
import { parse } from "fast-xml-parser";
import { createIntegerParser, createStringParser } from "./parserUtils";

const PERSON_HEADER = `PersonId;FirstName;LastName;Age;Color`;
const PERSON_FIELD_SEPARATOR = ";";
const PERSON_FIELDS = PERSON_HEADER.split(PERSON_FIELD_SEPARATOR);

const PERSON_FIELDPARSER = {
  PersonId: createIntegerParser(),
  FirstName: createStringParser(),
  LastName: createStringParser(),
  Age: createIntegerParser(),
  Color: createStringParser(),
};

export function parsePersonTXT(path: string): PersonDTO[] {
  const params: txtParserParams = {
    expected_header: PERSON_HEADER,
    columns: PERSON_FIELDS,
    field_separator: PERSON_FIELD_SEPARATOR,
    path,
    field_parsers: PERSON_FIELDPARSER,
  };
  return txtParser(params);
}

export function parsePersonXML(path: string): PersonDTO[] {
  const file = readFileSync(path);

  const json_object = parse(file.toString(), {
    attributeNamePrefix: "",
    ignoreAttributes: false,
  });

  const persons: PersonDTO[] = json_object.Data.Persons.Person.map(
    (unparsed_person: any) => {
      const parsed_person: Partial<PersonDTO> = {};
      const entries = Object.entries(unparsed_person);

      entries.forEach(([xml_field, value]) => {
        const parsed_field_name = fieldNameConverter(xml_field);
        const field_parser = PERSON_FIELDPARSER[parsed_field_name];
        parsed_person[parsed_field_name] = field_parser(value);
      });

      return <PersonDTO>parsed_person;
    }
  );
  return persons;
}

function fieldNameConverter(xml_field_name: string): string {
  return xml_field_name === "age" ? "Age" : xml_field_name;
}
