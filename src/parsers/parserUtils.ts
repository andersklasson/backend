export function createIntegerParser(): (unparsed_value: string) => number {
  return (unparsed_value: string): number => {
    const parsed_value = parseInt(unparsed_value);
    return parsed_value;
  };
}

export function createStringParser(): (unparsed_value: string) => string {
  return (unparsed_value): string => {
    const parsed = unparsed_value.trim();
    return parsed;
  };
}
