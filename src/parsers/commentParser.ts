import txtParser, { txtParserParams } from "./txtParser";
import { createIntegerParser, createStringParser } from "./parserUtils";
import { CommentDTO } from "../models";

const COMMENT_HEADER = "PersonId	Comment";
const COMMENT_SEPARATOR = "\t";
const COMMENT_FIELDS = COMMENT_HEADER.split(COMMENT_SEPARATOR);

export function parseCommentsTXT(path: string): CommentDTO[] {
  const params: txtParserParams = {
    columns: COMMENT_FIELDS,
    field_parsers: {
      PersonId: createIntegerParser(),
      Comment: createStringParser(),
    },
    path,
    field_separator: COMMENT_SEPARATOR,
    expected_header: COMMENT_HEADER,
  };
  return txtParser(params);
}
