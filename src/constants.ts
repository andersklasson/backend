const PATH_TO_FOLDER = `${__dirname}../../input_files`;
export const DATA_1_PATH = `${PATH_TO_FOLDER}/Data1.txt`;
export const DATA_2_PATH = `${PATH_TO_FOLDER}/Data2.xml`;
export const DATA_3_PATH = `${PATH_TO_FOLDER}/Data3..txt`;
